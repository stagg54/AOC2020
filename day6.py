def count_ind_group(group):
    members=group.split()
    s=set()
    for member in members:
        for Q in member:
            s.add(Q)
    return len(s)

def part1():
    f=open('day6.txt')
    data=f.readlines()
    f.close()
    groups="".join(data).split('\n\n')
    count=0
    for group in groups:
        count+=count_ind_group(group)
    return count

def count_part_2(group):
    members=group.split()
    d={}
    count=0
    for member in members:
        for Q in member:
            d.setdefault(Q,0)
            d[Q]+=1
    for key in d.keys():
        if d[key] == len(members):
            count+=1
    return count
def part2():
    f=open('day6.txt')
    data=f.readlines()
    f.close()
    groups="".join(data).split('\n\n')
    count=0
    for group in groups:
        count+=count_part_2(group)
    return count
