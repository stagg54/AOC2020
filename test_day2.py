import day2

def test_day2():
    testvector=('1-3 a: abcde','1-3 b: cdefg','2-9 c: ccccccccc')
    numvalid=day2.countvalidlines(testvector)
    assert numvalid == 1

"""
def test_day2():
    testvector=('1-3 a: abcde','1-3 b: cdefg','2-9 c: ccccccccc')
    numvalid=day2.countvalidlines(testvector)
    assert numvalid == 2
"""