import day3

testfile=open('day3testvector.txt')
test_vector = testfile.readlines()
test_vector = [x.strip() for x in test_vector]
testfile.close()
def test_find_trees():
    print(test_vector)
    assert day3.find_trees(test_vector,3,1) == 7

def test_check_slopes():
    slopes=[(1,1),(3,1),(5,1),(7,1),(1,2)]
    assert day3.check_slopes(test_vector,slopes) == 336
