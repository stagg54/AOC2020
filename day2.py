"""def isvalid(line):
     [policy, letter, password] = line.split()
     letter=letter[0]
     histogram={}
     for character in password:
         histogram.setdefault(character,0)
         histogram[character] += 1
     [min, max] = policy.split('-')
     count=histogram.get(letter,0)
     return count in range(int(min),int(max)+1)
"""
def countvalidlines(lines):
    count=0
    for line in lines:
        if isvalid(line):
            count+=1
    return count


def isvalid(line):
    [policy, letter, password] = line.split()
    letter = letter[0]
    [pos1, pos2] = policy.split('-')
    pos1=int(pos1)-1
    pos2=int(pos2)-1
    valid=(password[pos1]==letter) ^ (password[pos2] == letter)
    return valid
