import math
def find_trees(lines, dx, dy):
     x=0
     y=0
     count=0
     while y<len(lines):
         print(str(x) + ' ' + str(y))
         #note switched x and y
         if lines[y][x] == '#':
             print('tree')
             count += 1
         x+=dx
         x = x % len(lines[1])
         y+=dy
     return count

def  check_slopes(lines, slopes):
    trees=[]
    for slope in slopes:
        trees.append(find_trees(lines, slope[0], slope[1]))
    return math.prod(trees)