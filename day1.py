"""
Day 1 of Advent of Code 2020
"""
import itertools
import math


def fix_report(report, combo_size):
    """finds combo that sums to 2020 and returns product"""
    product = 0
    combos = list(itertools.combinations(report, combo_size))
    for combo in combos:
        if sum(list(combo)) == 2020:
            product = math.prod(list(combo))
    return product
