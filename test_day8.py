import day8
def test_part1():
    assert day8.part1('day8test.txt') == 5

def test_part2():
    assert day8.part2('day8test.txt') == 8