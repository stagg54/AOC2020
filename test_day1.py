import day1

def test_example1():
    input=[1721,979,366,299,675,1456]
    result=day1.fix_report(input,2)
    assert result == 514579


def test_example2():
    input=[1721,979,366,299,675,1456]
    result=day1.fix_report(input,3)
    assert result == 241861950

