
import itertools
import collections

def part2():
    f=open('day9.txt')
    data=[int(x) for x in f.readlines()]
    return find_cont(data,validate(data,25))

def part1():
    f=open('day9.txt')
    data=[int(x) for x in f.readlines()]
    return validate(data,25)

def validate(data,preamble):
    d = collections.deque(maxlen=preamble)
    predata=data[:preamble]
    postdata=data[preamble:]
    for point in predata:
        d.append(point)
    for point in postdata:
        valid_points = get_valid_points(d)
        if point in valid_points:
            d.append(point)
        else:
            return point


def get_valid_points(d):
    combos = list(itertools.combinations(list(d), 2))
    valid_points = list(set([x + y for (x, y) in combos]))
    return valid_points

def find_cont(data,target):
    done=False
    i=2
    while True:
        d=collections.deque(maxlen=i)
        predata=data[:i-1]
        postdata=data[i-1:]
        for j in predata:
            d.append(j)
        for k in postdata:
            d.append(k)
            if sum(list(d)) == target:
                done=True
                break
        if done:
            break
        i += 1
    return max(list(d))+min(list(d))