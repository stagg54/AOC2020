def get_seat_info(data):
    rowdata=data[0:7]
    row=0
    for i in range (7):
        if rowdata[i] == 'B':
            row += 2**(6-i)
    col=0
    coldata=data[7:10]
    for j in range (3):
        if coldata[j] == 'R':
            col += 2**(2-j)
    seat_id=row*8+col
    return [row,col,seat_id]

def part1():
    f=open('day5.txt')
    data=f.readlines()
    f.close()
    max=0
    for ticket in data:
        [row, col, seat_id]=get_seat_info(ticket)
        if seat_id > max:
            max=seat_id
    return max

def part2():
    f=open('day5.txt')
    data=f.readlines()
    f.close()
    actual_seat_ids=[]
    for ticket in data:
        [row,col, seat_id]=get_seat_info(ticket)
        actual_seat_ids.append(seat_id)
    all_seat_ids=list(range(849))
    missing_seat_ids=[id for id in all_seat_ids if id not in actual_seat_ids]
    myseat = [id for id in missing_seat_ids if int(id)-1 in actual_seat_ids and int(id)+1 in actual_seat_ids]
    return myseat[0]

def recurse(data, one, power=-1, count=-1):
    if power == -1:
        power = 0
    if count == -1:
        count = 0
    digit=data[-1]
    data=data[:-1]
    if digit == one:
        count += 2**power
    power += 1
    if len(data)>0:
        count=recurse(data,one, power, count)
    return count
