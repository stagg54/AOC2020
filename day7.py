
import re

def find_num_carriers(file, innerbag):
     rules=create_rules_dict(file)
     count=recurse(rules,innerbag)
     return count

def find_num_inners(file, carrier):
     rules=create_rules_dict(file)
     count=recurse_part2(rules, carrier)
     return count

def recurse(rules, innerbag, carriers_set=None):
     if carriers_set== None:
          carriers_set=set()
     for potential_carrier in list(rules.keys()):
          if innerbag in list(rules[potential_carrier].keys()):
               carriers_set.add(potential_carrier)
               recurse(rules, potential_carrier, carriers_set)
     return len(carriers_set)

def recurse_part2(rules, carrier):
    total=sum(list(rules[carrier].values()))
    for bag in list(rules[carrier].keys()):
         total+=rules[carrier][bag]*recurse_part2(rules,bag)
    return total




def create_rules_dict(file):
     f=open(file)
     data = f.readlines()
     f.close()
     rules={}
     for line in data:
          this_entry={}
          rule=re.split(r'\sbags contain\s', line)
          carrier=rule[0]
          #print(rule)
          #print(rule[1])
          if rule[1] =='no other bags.\n' or rule[1] == 'no other bags.':
               #print('match')
               rules[carrier]={}
          else:
               #print('no match')
               inners=rule[1].split(', ')
               for inner in inners:
                   innerdata=inner.split()
                   #print(innerdata)
                   innernum=int(innerdata[0])
                   innercolor=" ".join(innerdata[1:-1])
                   this_entry[innercolor]=innernum
               rules[carrier]=this_entry
     return rules

def part1():
     return find_num_carriers('day7.txt','shiny gold')
def part2():
     return find_num_inners('day7.txt','shiny gold')
