class SysState():
    def __init__(self, program):
        self.acc=0
        self.pointer=0
        self.program=program
        self.visited_steps=[]
        self.done=False
        self.success=False

    def exec_next(self):
        self.visited_steps.append(self.pointer)
        cmd=self.program[self.pointer]
        instr=cmd[:3]
        arg=int(cmd[4:])
        print ('Instr: ' + instr + ' Arg: ' + str(arg))
        if instr == 'acc':
            self.f_acc(arg)
        elif instr == 'nop':
            self.f_nop(arg)
        elif instr == 'jmp':
            self.f_jmp(arg)
        if self.pointer in self.visited_steps:
            self.done = True
        if self.pointer ==len(self.program):
            self.success = True
            self.done = True




    def f_acc(self, arg):
        self.acc += arg
        self.pointer += 1

    def f_nop(self,arg):
        self.pointer += 1

    def f_jmp(self, arg):
        self.pointer += arg

    def get_pointer(self):
        return self.pointer

    def get_acc(self):
        return self.acc

    def is_done(self):
        return self.done

    def worked(self):
        return self.success

def part1(input):
    f=open(input)
    program=f.readlines()
    f.close()
    sys=SysState(program)
    while not sys.is_done():
        #print('Pointer: '+str(sys.get_pointer()) +' Accumulator: '+str(sys.get_acc()))
        sys.exec_next()
    return sys.get_acc()

def part2(input):
    f=open(input)
    program=f.readlines()
    f.close()
    result=None
    print('original Prog')
    print(program)
    nops = [x for x in range(len(program)) if program[x][:3] == 'nop' ]
    print('nops')
    print(nops)
    jmps = [x for x in range(len(program)) if program[x][:3] == 'jmp' ]
    print('jmps')
    print(jmps)
    for cmd in nops:
        newprogram = program[::]
        newprogram[cmd]=newprogram[cmd].replace('nop','jmp')
        print(newprogram)
        sys=SysState(newprogram)

        while not sys.is_done():
           print('Pointer: '+str(sys.get_pointer()) +' Accumulator: '+str(sys.get_acc()))
           sys.exec_next()
        if sys.worked():
               result=sys.get_acc()
    if result == None:
        for cmd in jmps:
            newprogram = program[:]
            newprogram[cmd]=newprogram[cmd].replace('jmp', 'nop')
            print(newprogram)
            sys = SysState(newprogram)

            while not sys.is_done():
                print('Pointer: '+str(sys.get_pointer()) +' Accumulator: '+str(sys.get_acc()))
                sys.exec_next()
            if sys.worked():
                result = sys.get_acc()
    return result



