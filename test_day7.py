import day7

def test_create_rules_dict():
    rules=day7.create_rules_dict('day7shorttest.txt')
    desired={'light red':{'bright white':1,'muted yellow':2},'dark orange':{'bright white':3,'muted yellow':4},'dotted black':{}}
    assert rules == desired

def test_find_num_carriers():
    assert day7.find_num_carriers('day7test.txt','shiny gold') == 4
def test_find_num_inners():
    assert day7.find_num_inners('day7test.txt','shiny gold') == 32
    assert day7.find_num_inners('day7test2.txt', 'shiny gold') == 126
