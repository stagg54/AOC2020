import day5

def test_get_seat_info():
     assert day5.get_seat_info('FBFBBFFRLR') == [44, 5, 357]
     assert day5.get_seat_info('BFFFBBFRRR') == [70, 7, 567]
     assert day5.get_seat_info('FFFBBBFRRR') == [14, 7, 119]
     assert day5.get_seat_info('BBFFBBFRLL') == [102, 4, 820]


