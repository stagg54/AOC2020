import day9
import collections

def test_get_valid_points():
    d=collections.deque(maxlen=4)
    for i in[1,2,3,4]:
        d.append(i)
    assert day9.get_valid_points(d)==[3,4,5,6,7]

def test_validate():
    f=open('day9test.txt')
    rawdata=f.readlines()
    f.close()
    data=[int(x) for x in rawdata ]
    assert day9.validate(data,5) == 127

def test_find_cont():
    f=open('day9test.txt')
    rawdata=f.readlines()
    f.close()
    data=[int(x) for x in rawdata ]
    assert day9.find_cont(data,127)==62