import day6
def test_count_ind_group():
    assert day6.count_ind_group('abc') == 3
    assert day6.count_ind_group('a\nb\nc') == 3
    assert day6.count_ind_group('ab\nac') == 3
    assert day6.count_ind_group('a\na\na\na\n') == 1
    assert day6.count_ind_group('b') == 1
def test_count2():
    assert day6.count_part_2('abc') == 3
    assert day6.count_part_2('a\nb\nc') == 0
    assert day6.count_part_2('ab\nac') == 1
    assert day6.count_part_2('a\na\na\na\n') == 1
