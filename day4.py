import re

required_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
hair_regex = re.compile(r'#[01234567689abcdef]{6}')


def load_passport_file(filename):
    f = open(filename)
    filedata = f.readlines()
    passports = "".join(filedata)
    passports = passports.split('\n\n')
    f.close()
    return passports

def get_fields(passport):
    pairs = passport.split()
    fields = {}
    for pair in pairs:
        [key, value] = pair.split(':')
        fields[key] = value

def validate_passport(passport):
    fields = get_fields(passport)
    valid = True
    if all_required_fields_present(fields):
        for key in list(fields.keys()):
            if not is_field_data_valid(key, fields[key]):
                valid = False
    else:
        valid = False
    return valid


def all_required_fields_present(fields):
    valid = True
    for field in required_fields:
        if field not in fields:
            valid = False
            break
    return valid

def birth(data):
    return len(data) == 4 and data.isdigit() and 1920 <= int(data) and 2002 >= int(data)

def issue(data):
    return len(data) == 4 and data.isdigit() and 2010 <= int(data) and 2020 >= int(data)

def expiration(data):
    return len(data) == 4 and data.isdigit() and 2020 <= int(data) and 2030 >= int(data)
def height(data):
    if data[-2:] == 'in':
        return data[:-2].isdigit() and 59 <= int(data[:-2]) and 76 >= int(data[:-2])
    elif data[-2:] == 'cm':
        return data[:-2].isdigit() and 150 <= int(data[:-2]) and 193 >= int(data[:-2])
    else:
        return False
def hair(data):
    return hair_regex.search(data) != None
def eye(data):
    return data in ['amb','blu','brn','gry','grn','hzl','oth']
def pid(data):
    return data.isdigit() and len(data) == 9

def is_field_data_valid (key, value):
    switcher = {
               'byr':birth(value),
               'iyr':issue(value),
               'eyr':expiration(value),
               'hgt':height(value),
               'hcl':hair(value),
               'ecl':eye(value),
               'pid':pid(value),
               'cid':True
    }
    return switcher[key]

def get_fields(passport):
    pairs = passport.split()
    fields = {}
    for pair in pairs:
        [key, value] = pair.split(':')
        fields[key] = value
    return fields


def part1():
    passports = load_passport_file('day4.txt')
    count = 0
    for passport in passports:
      fields=get_fields(passport)
      if all_required_fields_present(fields):
        count+=1
    return count

def part2():
    passports = load_passport_file('day4.txt')
    count = 0
    for passport in passports:
       if validate_passport(passport):
           count+=1
    return count
