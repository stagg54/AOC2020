import day4
'''
#part 1 tests
passports = day4.load_passport_file('day4testvector.txt')
def test_loadfile():
    assert len(passports) == 4

def test_validate_passport_part1():
    assert day4.validate_passport(passports[0]) == True
    assert day4.validate_passport(passports[1]) == False
    assert day4.validate_passport(passports[2]) == True
    assert day4.validate_passport(passports[3]) == False
'''

def test_byr():
    assert day4.birth('2002')
    assert not day4.birth('2003')

def test_hgt():
    assert day4.height('60in')
    assert day4.height('190cm')
    assert not day4.height('190in')
    assert not day4.height('190')

def test_hcl():
    assert day4.hair('#123abc')
    assert not day4.hair('#123abz')
    assert not day4.hair('123abc')

def test_ecl():
    assert day4.eye('brn')
    assert not day4.eye('wat')

def test_pid():
    assert day4.pid('000000001')
    assert not day4.pid('0123456789')

def test_valid_passports():
    passports = day4.load_passport_file('day4testvalid.txt')
    for passport in passports:
        assert day4.validate_passport(passport)

def test_invalid_passports():
    passports = day4.load_passport_file('day4testinvalid.txt')
    for passport in passports:
        assert not day4.validate_passport(passport)

